//
//  PeripheralCViewController.swift
//  CoreBluetoothTest
//
//  Created by Mackwell on 12/01/2016.
//  Copyright © 2016 Mackwell. All rights reserved.
//

import UIKit
import CoreBluetooth


let MASTER_ID_USER:UInt8 = 0x02



var txBuffer = [UInt8] (count: 20, repeatedValue: 0x00)
var snArray: [UInt8] = [0x91,0x92,0x93,0x94,0x00, 0x00, 0x00, 0x00]
var locationArray: [UInt8] = [0x6F,0x66,0x66,0x69,0x63,0x65,0x00,0x00]



func buildTxBuffer(action: UInt8, command: UInt8, address:UInt8) {
    txBuffer.removeAll()
    txBuffer.append(MASTER_ID_USER)
    txBuffer.append(action)
    txBuffer.append(command)
    txBuffer.append(address)
    
    txBuffer.appendContentsOf(snArray)
    txBuffer.appendContentsOf(locationArray)

    print(txBuffer)
    
}


class PeripheralCViewController: UITableViewController, CBCentralManagerDelegate,CBPeripheralDelegate {

    var centralManager: CBCentralManager?
    var device: CBPeripheral?
    var commandCharacteristic: CBCharacteristic?
    
    var switchOn = false
    var connected = false

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet var statusSwitch: UISwitch!
    @IBOutlet weak var readLabel: UILabel!
    @IBOutlet weak var writeLabel: UILabel!


    
    @IBAction func stateChanged(sender: UISwitch) {
        
        statusSwitch.enabled = false
        statusLabel.text = "Connecting"
        
        if !connected {
            centralManager?.connectPeripheral(device!, options: nil)
            statusSwitch.enabled = false
        } else {
            centralManager?.cancelPeripheralConnection(device!)
            
        }
        
    }
    

    func sendCommand(row: Int) {
        
        switch  row {
        case 0:
            if let char = self.commandCharacteristic {
                
                buildTxBuffer(TOGGLE, command: FT, address: 0x02)
                
                let data = NSData(bytes: txBuffer, length: txBuffer.count)
                device?.writeValue(data, forCharacteristic: char, type: CBCharacteristicWriteType.WithoutResponse)
            }
            
            writeLabel.text = "\(txBuffer)"
        case 1:
            if let char = self.commandCharacteristic {
                
                buildTxBuffer(TOGGLE, command: ST, address: 0x02)
                
                let data = NSData(bytes: txBuffer, length: txBuffer.count)
                device?.writeValue(data, forCharacteristic: char, type: CBCharacteristicWriteType.WithoutResponse)
                
            }
            
            writeLabel.text = "\(txBuffer)"
        case 2:
            if let char = self.commandCharacteristic {

                buildTxBuffer(SET, command: SN, address: 0x02)
                
                let data = NSData(bytes: txBuffer, length: txBuffer.count)
                device?.writeValue(data, forCharacteristic: char, type: CBCharacteristicWriteType.WithoutResponse)
            }
            
            writeLabel.text = "\(txBuffer)"
        case 3:
            if let char = self.commandCharacteristic {

                buildTxBuffer(GET, command: LOCATION, address: 0x02)
                
                let data = NSData(bytes: txBuffer, length: txBuffer.count)
                device?.writeValue(data, forCharacteristic: char, type: CBCharacteristicWriteType.WithoutResponse)
            }
            
            writeLabel.text = "\(txBuffer)"
        case 4:
            if let char = self.commandCharacteristic {
                
                buildTxBuffer(SET, command: LOCATION, address: 0x02)
                
                let data = NSData(bytes: txBuffer, length: txBuffer.count)
                device?.writeValue(data, forCharacteristic: char, type: CBCharacteristicWriteType.WithoutResponse)
            }
            
            writeLabel.text = "\(txBuffer)"
        case 5:
            if let char = self.commandCharacteristic {
                // Need a mutable var to pass to writeValue function
                buildTxBuffer(GET, command: STATUS, address: 0x02)
                
                let data = NSData(bytes: txBuffer, length: txBuffer.count)
                device?.writeValue(data, forCharacteristic: char, type: CBCharacteristicWriteType.WithoutResponse)
            }
            
            writeLabel.text = "\(txBuffer)"
            
        default: 0
            
        }
    }
    
    func popAlert(data: [UInt8]) {
        if #available(iOS 8.0, *) {
            
            let message = data[0] == 0 ? "OK" : "ERROR"
            
            let alertController = UIAlertController(title: "Status", message: message, preferredStyle: .Alert)
            
            alertController.addAction(UIAlertAction(title: "Dismiss", style: .Default, handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    
    
    
    // MARK: Life circle
    
    override func viewDidLoad() {
        
        device?.delegate = self
        centralManager?.delegate = self
        
//        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")
        
//        idButton.layer.borderWidth = 1
//        idButton.layer.cornerRadius = 8
//        idButton.layer.borderColor = UIColor.lightGrayColor().CGColor
//        
//        queryButton.layer.borderWidth = 1
//        queryButton.layer.cornerRadius = 8
//        queryButton.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(animated: Bool) {
        if connected {
            centralManager?.cancelPeripheralConnection(device!)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section {
        case 0: return 1
        case 1: return 2
        case 2: return 6
        default: return 0
        }
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if 2 == indexPath.section {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            sendCommand(indexPath.row)
            
        }
    }
    


    
    /*override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)
        
       cell.textLabel?.text = "test"
        

        return cell
    }*/


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        print("BLend Micro connected")
        connected = .Connected == peripheral.state ? true : false
        

//        peripheral.discoverServices([CBUUID(NSUUID: peripheral.identifier)])
        peripheral.discoverServices([])
        
    }
    
    func centralManager(central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        print("failed to connect")
    }
    
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        0
    }
    
    func centralManagerDidUpdateState(central: CBCentralManager) {
        0
    }
    
    func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        print("Disconnected:  \(peripheral.name)")
        connected = false
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            dispatch_async(dispatch_get_main_queue(), {
                self.statusLabel.text = "Disconnected"
                self.statusSwitch.enabled = true
                self.statusSwitch.on = false
            })
            
        }
    }
    
    
    
    
    // MARK: - CBPeripheralDelegate
    
    func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?) {
        
        if peripheral.services?.count > 0 {
            print("Discovered service: ", peripheral.services![0].UUID)
            
            peripheral.discoverCharacteristics(nil , forService: peripheral.services![0])

        }
        
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
        
        for aChar: CBCharacteristic in service.characteristics! {
            
            if CommandCharUUID == aChar.UUID {
                print("CommandCharUUID Value: ", aChar.value)
                commandCharacteristic = aChar
                
            } else if NotifyCharUUID == aChar.UUID {
                print("NotifyCharUUID Value: ", aChar.value)
                device?.setNotifyValue(true, forCharacteristic: aChar)
            }
            
            let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
            dispatch_async(dispatch_get_global_queue(priority, 0)) {
                dispatch_async(dispatch_get_main_queue(), {
                    self.statusLabel.text = "Connected"
                    self.statusSwitch.enabled = true
                })
                
            }

            
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        print(characteristic.value)
        
        let data = characteristic.value
        let count = data!.length /  sizeof(UInt8)
        var array = [UInt8](count: count, repeatedValue: 0)
        
        data!.getBytes(&array, length: count * sizeof(UInt8))
        print(array)
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            dispatch_async(dispatch_get_main_queue(), {
                self.readLabel?.text = "\(array)"
                self.popAlert(array)
            })
            
        }
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print("segue")  
    }
    
    
}
