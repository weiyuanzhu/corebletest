//
//  BleDevice.swift
//  CoreBluetoothTest
//
//  Created by Mackwell on 13/01/2016.
//  Copyright © 2016 Mackwell. All rights reserved.
//

import Foundation

struct BlePeripheral: Equatable {
    
    var name: String?
    var uuid: String?
    
    init(name: String?, uuid: String?) {
        self.name = name
        self.uuid = uuid
    }
    
}

func ==(lhs: BlePeripheral, rhs: BlePeripheral) -> Bool {
    return (lhs.name == rhs.name && lhs.uuid == rhs.uuid)
}
