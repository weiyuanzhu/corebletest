//
//  N_Light_Library.swift
//  CoreBluetoothTest
//
//  Created by Mackwell on 17/02/2016.
//  Copyright © 2016 Mackwell. All rights reserved.
//

import Foundation

let GET:UInt8         = 0xA0
let TOGGLE:UInt8      = 0xA1
let SET:UInt8         = 0xA2

let INIT: UInt8       = 0x21
let FT:UInt8          = 0x60
let SN:UInt8          = 0x8A
let LOCATION:UInt8    = 0x82

let DT: UInt8 =          0x61
let ST: UInt8  =        0x62
let ID: UInt8   =       0x65
let STOP_ID: UInt8 =      0x66
let STATUS: UInt8 =       0x6F