//
//  ViewController.swift
//  CoreBluetoothTest
//
//  Created by Mackwell on 08/01/2016.
//  Copyright © 2016 Mackwell. All rights reserved.
//

import UIKit
import CoreBluetooth
import QuartzCore

let BLEServiceUUID = CBUUID(string: "713d0000-503e-4c75-ba94-3148f18d941e")
let CommandCharUUID = CBUUID(string: "713d0003-503e-4c75-ba94-3148f18d941e")
let NotifyCharUUID = CBUUID(string: "713d0002-503e-4c75-ba94-3148f18d941e")
let BLEServiceChangedStatusNotification = "kBLEServiceChangedStatusNotification"


class ViewController: UIViewController, CBCentralManagerDelegate,CBPeripheralDelegate, UITableViewDataSource {
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var bleTableView: UITableView!
    @IBOutlet weak var bleLabel: UILabel!
    
    var centralManager: CBCentralManager?
    var bles: [CBPeripheral] = []
    var commandCharacteristic: CBCharacteristic?
    var bleSelected: CBPeripheral?
    
    var connected: Bool = false
    var items: [String] = []
    var uuid: [String] = []
    var peripherals: [BlePeripheral] = []


    @IBAction func searchBLE(sender: UIBarButtonItem) {
        
        bles.removeAll()
        bleTableView.reloadData()
        
        loadingIndicator.startAnimating()
        
        centralManager?.scanForPeripheralsWithServices([BLEServiceUUID], options: nil)
//        centralManager?.scanForPeripheralsWithServices(nil, options: nil)
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        bleTableView.registerClass(UITableViewCell.self , forCellReuseIdentifier: "Cell1234")

        
        let centralQueue = dispatch_queue_create("com.mackwell", DISPATCH_QUEUE_SERIAL)
        centralManager = CBCentralManager(delegate: self, queue: centralQueue)
        
        loadingIndicator.hidesWhenStopped = true

        
    }
    
    override func viewWillAppear(animated: Bool) {
        //take back delegation
        centralManager?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: CBCentralManagerDelegate
    
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        print("connected")
        peripheral.delegate = self
        connected = .Connected == peripheral.state ? true : false
        peripheral.discoverServices([BLEServiceUUID])
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            dispatch_async(dispatch_get_main_queue(), {
                self.loadingIndicator.stopAnimating()
            })
            
        }

        
    }
    
    func centralManager(central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        print("failed to connect")
    }
    
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        
        
        print("found: \(peripheral.name)")
        
        if !bles.contains(peripheral) {
            bles.append(peripheral)
        }
        
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            dispatch_async(dispatch_get_main_queue(), {
                self.bleTableView.reloadData()
                self.loadingIndicator.stopAnimating()
            })
            
            
        }
        
        
//        peripheral.readRSSI()
        

//        centralManager?.stopScan()
        
    }
    
    func centralManagerDidUpdateState(central: CBCentralManager) {
        0
    }
    
    func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        connected = false
        print("disconnected")
    }

    
    
    
    // MARK: CBPeripheralDelegate
    
    func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?) {
        print("didDescoverServices")
        let service = peripheral.services?[0]
        
        print("Discovered service: ", service?.UUID)
        
        peripheral.discoverCharacteristics(nil , forService: service!)
        
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
       
    }
    
    func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        print(characteristic.value)
        
        let data = characteristic.value
        let count = data!.length /  sizeof(UInt8)
        var array = [UInt8](count: count, repeatedValue: 0)
        
        data!.getBytes(&array, length: count * sizeof(UInt8))
        
        let dataString = String(data: data!, encoding: NSUTF8StringEncoding)
        print(array)
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            dispatch_async(dispatch_get_main_queue(), {
                self.bleLabel?.text = "BLE Message: " + dataString!
            })
            
        }
        
    }
    
    // MARK: TabkeView Delegate and DataSource
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = bleTableView.dequeueReusableCellWithIdentifier("Cell1234")!
        
        if let name = bles[indexPath.row].name{
            cell.textLabel?.text = name
        } else {
            cell.textLabel?.text = "n/a"

        }
        cell.detailTextLabel?.text = bles[indexPath.row].identifier.UUIDString
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bles.count
    }
    


    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        print("You selected cell #\(indexPath.row)!")
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        bleSelected = bles[indexPath.row]
        
        performSegueWithIdentifier("showPeripheral", sender: self)
    }
    
    // MARK: Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let destController = segue.destinationViewController as? PeripheralCViewController {
            destController.centralManager = self.centralManager
            destController.device = self.bleSelected
        }
        
        centralManager?.stopScan()
    }
    
    @IBAction func unwind(sender: UIStoryboardSegue) {

    }
    
  
}

